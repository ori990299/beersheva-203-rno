﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ServerUDP
{
    class Program
    {
        public static void Main()
        {
            UDPListner udpListner = new UDPListner(9050);

            udpListner.startRedirectPackets();
        }
    }

    class UDPListner
    {
        private byte[] data { get; set; }
        private IPEndPoint ipep { get; set; }
        private UdpClient newsock { get; set; }
        private Thread redirectPacketsThread { get; set; }
        private Dictionary<string, IPEndPoint> redirectDictionary { get; set; }
        private Boolean redirect { get; set; }
        public UDPListner(int port)
        {
            data = new byte[1024];
            ipep = new IPEndPoint(IPAddress.Any, port); //choosing server ip and port
            newsock = new UdpClient(ipep);// creaing new socket
            redirectPacketsThread = new Thread(redirectPackets);
            redirectDictionary = new Dictionary<string, IPEndPoint>();
        }

        public void startRedirectPackets()
        {
            redirect = true;
            redirectPacketsThread.Start();
        }

        public void stopRedirectPackets()
        {
            redirect = false;
        }

        public void addRedirectAddresses(IPAddress AIP, int Aport, IPAddress BIP, int Bport)
        {
            redirectDictionary.Add(AIP.ToString() + ":" + Aport.ToString(), new IPEndPoint(BIP, Bport));
            redirectDictionary.Add(BIP.ToString() + ":" + Bport.ToString(), new IPEndPoint(AIP, Aport));
        }

        public void removeRedirectAddresses(IPAddress AIP, int Aport, IPAddress BIP, int Bport)
        {
            redirectDictionary.Remove(AIP.ToString() + ":" + Aport.ToString());
            redirectDictionary.Remove(BIP.ToString() + ":" + Bport.ToString());
        }

        private void redirectPackets()
        {
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);// latest packet sender

            data = newsock.Receive(ref sender);//wait for a new message

            IPAddress AIP = sender.Address;
            int Aport = sender.Port;

            Console.WriteLine("GOT A");

            IPAddress BIP;
            int Bport;

            do
            {
                data = newsock.Receive(ref sender);//wait for a new message

                BIP = sender.Address;
                Bport = sender.Port;

            } while(Bport == Aport);

            Console.WriteLine("GOT B");

            addRedirectAddresses(AIP, Aport, BIP, Bport);// add the adresses and ports to the redirect dictionary

            while (redirect)
            {
                data = newsock.Receive(ref sender);//wait for a new message

                try
                {
                    Console.WriteLine("from: " + sender.ToString() + " to: " + redirectDictionary[sender.ToString()]);
                    newsock.Send(data, data.Length, redirectDictionary[sender.ToString()]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

    }
}
