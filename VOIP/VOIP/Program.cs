﻿using System;
using System.Collections.Generic;
using NAudio.Wave;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace VOIP
{
    class Program
    {
        enum program
        {
            playBack,
            Send,
            Receive,
            Call
        }

        static void Main(string[] args)
        {
            Console.WriteLine(IPAddress.Any);

            Console.WriteLine("Please choose what program you want to run:");
            foreach (int i in Enum.GetValues(typeof(program)))// prints all the enum in format: {number} - {program}
            {
                Console.WriteLine($"{i} - {(program)i}");
            }

            program programToRun = (program)Convert.ToInt32(Console.ReadLine());

            switch(programToRun)
            {
                case program.playBack:
                    playBackDevice();
                    break;
                case program.Send:
                    sendDeviceSound();
                    break;
                case program.Receive:
                    receiveDeviceSound();
                    break;
                case program.Call:
                    startCall();
                    break;
            }
        }

        static void playBackDevice()
        {
            List<WaveInCapabilities> sources = new List<WaveInCapabilities>();// lit for the input devices

            Console.WriteLine("which device would you like to playback:");

            for (int i = 0; i < WaveIn.DeviceCount; i++)// gets all the devices + print them all out
            {
                var device = WaveIn.GetCapabilities(i);
                sources.Add(device);
                Console.WriteLine(i + ": " + device.ProductName);
            }

            Console.Write("enter device number here: ");
            int deviceNumber = Convert.ToInt32(Console.ReadLine());// gets the number of the device the user wants to use

            WaveInEvent sourceStream = new WaveInEvent();
            sourceStream.DeviceNumber = deviceNumber;// sets the sourceStream to the selected device (the device the user picked)
            sourceStream.WaveFormat = new WaveFormat(4410, WaveIn.GetCapabilities(deviceNumber).Channels);

            WaveInProvider waveIn = new WaveInProvider(sourceStream);

            DirectSoundOut waveOut = new DirectSoundOut();
            waveOut.Init(waveIn);

            sourceStream.StartRecording();
            waveOut.Play();

            Console.Write("Press any key to stop...");

            Console.ReadLine();// wait for any key

            waveOut.Stop();
            waveOut.Dispose();

            sourceStream.StopRecording();
            sourceStream.Dispose();
        }

        static void sendDeviceSound()
        {
            List<WaveInCapabilities> sources = new List<WaveInCapabilities>();// lit for the input devices

            Console.WriteLine("which device input would you like to send:");

            for (int i = 0; i < WaveIn.DeviceCount; i++)// gets all the devices + print them all out
            {
                var device = WaveIn.GetCapabilities(i);
                sources.Add(device);
                Console.WriteLine(i + ": " + device.ProductName);
            }

            Console.Write("enter device number here: ");
            int deviceNumber = Convert.ToInt32(Console.ReadLine());// gets the number of the device the user wants to use

            //Audio format
            WaveFormat format = new WaveFormat(44400, 2);

            //Audio input
            WaveInEvent sourceStream = new WaveInEvent();
            sourceStream.DeviceNumber = deviceNumber;// sets the sourceStream to the selected device (the device the user picked)
            sourceStream.WaveFormat = format;//sets the wave format

            //Audio buffer
            BufferedWaveProvider bufferToSend = new BufferedWaveProvider(format);

            UdpClient udpClient = new UdpClient();
            udpClient.Connect("176.230.211.127", 9050);
            Byte[] sendBytes = Encoding.ASCII.GetBytes("Is anybody there?");
            udpClient.Send(sendBytes, sendBytes.Length);

            sourceStream.DataAvailable += (object sender, WaveInEventArgs args) =>
            {
                //bufferToSend.AddSamples(args.Buffer, 0, args.BytesRecorded);
                udpClient.Send(args.Buffer, args.BytesRecorded);
                Console.WriteLine(args.BytesRecorded);
                Console.WriteLine("newDataAvailable");
            };

            sourceStream.StartRecording();

            Console.Write("Press any key to stop...");

            Console.ReadLine();// wait for any key

            sourceStream.StopRecording();
            sourceStream.Dispose();

            udpClient.Close();
            udpClient.Dispose();
        }

        static void receiveDeviceSound()
        {
            int recv;
            byte[] data = new byte[1024];
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);

            Socket newsock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            newsock.Bind(ipep);
            Console.WriteLine("Waiting for a client...");

            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint Remote = (EndPoint)(sender);

            BufferedWaveProvider provider = new BufferedWaveProvider(new WaveFormat(44400, 2));
            provider.DiscardOnBufferOverflow = true;

            recv = newsock.ReceiveFrom(data, ref Remote);

            Console.WriteLine("Message received from {0}:", Remote.ToString());
            Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));

            Console.WriteLine("Welcome to my test server");

            DirectSoundOut waveOut = new DirectSoundOut();
            waveOut.Init(provider);
            waveOut.Play();

            while (true)
            {
                data = new byte[17760];
                recv = newsock.ReceiveFrom(data, ref Remote);
                Console.WriteLine("newDataAvailable");
                provider.AddSamples(data, 0, data.Length);
            }

            waveOut.Stop();
            waveOut.Dispose();
        }

        static void startCall()
        {
            UdpClient udpClient = new UdpClient();
            udpClient.Connect("176.230.211.127", 9050);

            byte[] data = new byte[1024];

            List<WaveInCapabilities> sources = new List<WaveInCapabilities>();// lit for the input devices

            Console.WriteLine("which device input would you like to send:");

            for (int i = 0; i < WaveIn.DeviceCount; i++)// gets all the devices + print them all out
            {
                var device = WaveIn.GetCapabilities(i);
                sources.Add(device);
                Console.WriteLine(i + ": " + device.ProductName);
            }

            Console.Write("enter device number here: ");
            int deviceNumber = Convert.ToInt32(Console.ReadLine());// gets the number of the device the user wants to use

            //Audio format
            WaveFormat format = new WaveFormat(44400, 2);

            //Audio input
            WaveInEvent sourceStream = new WaveInEvent();
            sourceStream.DeviceNumber = deviceNumber;// sets the sourceStream to the selected device (the device the user picked)
            sourceStream.WaveFormat = format;//sets the wave format

            sourceStream.DataAvailable += (object sender, WaveInEventArgs args) =>
            {
                udpClient.Send(args.Buffer, args.BytesRecorded);
                Console.WriteLine(args.BytesRecorded);
                Console.WriteLine("newDataAvailable");
            };

            BufferedWaveProvider provider = new BufferedWaveProvider(new WaveFormat(44400, 2));
            provider.DiscardOnBufferOverflow = true;

            DirectSoundOut waveOut = new DirectSoundOut();
            waveOut.Init(provider);
            waveOut.Play();

            Byte[] sendBytes = Encoding.ASCII.GetBytes("Is anybody there?");
            udpClient.Send(sendBytes, sendBytes.Length);

            Console.Write("Press any key to start.");
            Console.ReadLine();// wait for any key
            udpClient.Send(sendBytes, sendBytes.Length);

            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

            sourceStream.StartRecording();

            while (true)
            {
                data = new byte[17760];
                data = udpClient.Receive(ref sender);
                Console.WriteLine("newDataAvailable");
                provider.AddSamples(data, 0, data.Length);
            }

            waveOut.Stop();
            waveOut.Dispose();

            Console.Write("Press any key to stop...");

            Console.ReadLine();// wait for any key

            sourceStream.StopRecording();
            sourceStream.Dispose();

            udpClient.Close();
            udpClient.Dispose();
        }
    }
}

//while (true)
//{
//    if (Console.ReadKey().Key == ConsoleKey.Enter)
//    {
//        break;
//    }
//}
