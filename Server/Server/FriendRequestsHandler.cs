﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Server
{
    class FriendRequestsHandler
    {
        const int GET_FRIENDS_REQUEST = 110;
        const int GET_FRIENDS_RESPONSE = 111;
        const int SEND_FRIEND_REQUEST = 120;
        const int SEND_FRIEND_RESPONSE = 121;
        const int SEND_FRIEND_REQUEST_TO_CLIENT = 123;
        const int ACCEPT_FRIEND_REQUEST = 130;
        const int ACCEPT_FRIEND_RESPONSE = 131;
        const int DENY_FRIEND_REQUEST = 140;
        const int DENY_FRIEND_RESPONSE = 141;
        const int GET_ALL_FRIEND_REQUESTS = 150;
        const int GET_ALL_FRIEND_RESPONSE = 151;
        const int ERROR_ADDER = 5;

        private ErrorResponse errorResponse;
        private SQLHandler SQLHandler;

        public FriendRequestsHandler()
        {
            this.errorResponse = new ErrorResponse(0, "");
            this.SQLHandler = new SQLHandler();
        }
        public string GetFriends(GetFriendFormat getFriendFormat)
        {
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            if (SQLHandler.checkIfUserExists(getFriendFormat.username))
            {
                string friends = SQLHandler.GetFriends(getFriendFormat.username);
                responseFormat.code = GET_FRIENDS_RESPONSE;
                responseFormat.content = friends;
                return JsonConvert.SerializeObject(responseFormat);
            }
            else
            {
                errorResponse.code = GET_FRIENDS_REQUEST + ERROR_ADDER;
                errorResponse.content = "username does not exist";
                return JsonConvert.SerializeObject(errorResponse);
            }
        }

        public string DenyFriendRequest(FriendRequest denyRequest, Dictionary<string, LinkedList<string>> friendsRequests)
        {
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            if (SQLHandler.checkIfUserExists(denyRequest.sender) && SQLHandler.checkIfUserExists(denyRequest.recipent))
            {
                if (friendsRequests.ContainsKey(denyRequest.recipent))
                {
                    if (friendsRequests[denyRequest.recipent].Find(denyRequest.sender) != null)
                    {
                        friendsRequests[denyRequest.recipent].Remove(denyRequest.sender);
                        responseFormat.code = DENY_FRIEND_RESPONSE;
                        responseFormat.content = denyRequest.sender;
                        return JsonConvert.SerializeObject(responseFormat);
                    }
                    else
                    {
                        errorResponse.code = DENY_FRIEND_REQUEST + ERROR_ADDER;
                        errorResponse.content = "you haven't sent a friend request!";
                        return JsonConvert.SerializeObject(errorResponse);
                    }
                }
                else
                {
                    errorResponse.code = DENY_FRIEND_REQUEST + ERROR_ADDER;
                    errorResponse.content = "you haven't sent a friend request!";
                    return JsonConvert.SerializeObject(errorResponse);
                }
            }
            else
            {
                errorResponse.code = DENY_FRIEND_REQUEST + ERROR_ADDER;
                errorResponse.content = "username does not exist";
                return JsonConvert.SerializeObject(errorResponse);
            }
        }

        public string GetAllFriendsRequests(GetAllFriendsRequests getAllFriendsRequests, Dictionary<string, LinkedList<string>> friendsRequests)
        {
            Helper Helper = new Helper();
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            if (SQLHandler.checkIfUserExists(getAllFriendsRequests.username))
            {
                if (friendsRequests.ContainsKey(getAllFriendsRequests.username))
                {
                    responseFormat.code = GET_ALL_FRIEND_RESPONSE;
                    responseFormat.content = Helper.ListToString(friendsRequests[getAllFriendsRequests.username]);
                    return JsonConvert.SerializeObject(responseFormat);
                }
                else
                {
                    errorResponse.code = GET_ALL_FRIEND_REQUESTS + ERROR_ADDER;
                    errorResponse.content = "you have no pending friend requests";
                    return JsonConvert.SerializeObject(errorResponse);
                }
            }
            else
            {
                errorResponse.code = GET_ALL_FRIEND_REQUESTS + ERROR_ADDER;
                errorResponse.content = "username does not exist";
                return JsonConvert.SerializeObject(errorResponse);
            }
        }

        public string[] AcceptFriendRequest(FriendRequest acceptRequest, Dictionary<string, LinkedList<string>> friendsRequests)
        {
            Helper Helper = new Helper();
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            string[] res = new string[2];
            if (SQLHandler.checkIfUserExists(acceptRequest.sender) && SQLHandler.checkIfUserExists(acceptRequest.recipent))
            {
                if (Helper.IfFriends(acceptRequest.sender, acceptRequest.recipent))
                {
                    errorResponse.code = ACCEPT_FRIEND_REQUEST + ERROR_ADDER;
                    errorResponse.content = "you and " + acceptRequest.sender + " already friends!";
                    res[0] = JsonConvert.SerializeObject(errorResponse);
                    res[1] = null;
                    return res;
                }
                else
                {
                    if (friendsRequests.ContainsKey(acceptRequest.recipent))
                    {
                        if (friendsRequests[acceptRequest.recipent].Find(acceptRequest.sender) != null)
                        {
                            SQLHandler.AddFriend(acceptRequest.sender, acceptRequest.recipent);
                            SQLHandler.AddFriend(acceptRequest.recipent, acceptRequest.sender);
                            friendsRequests[acceptRequest.recipent].Remove(acceptRequest.sender);
                            responseFormat.code = ACCEPT_FRIEND_RESPONSE;
                            responseFormat.content = acceptRequest.sender;
                            res[0] = JsonConvert.SerializeObject(responseFormat);
                            responseFormat.code = ACCEPT_FRIEND_RESPONSE;
                            responseFormat.content = acceptRequest.recipent;
                            res[1] = JsonConvert.SerializeObject(responseFormat);
                            return res;
                        }
                        else
                        {
                            errorResponse.code = ACCEPT_FRIEND_REQUEST + ERROR_ADDER;
                            errorResponse.content = "you dont have a pending friend request from " + acceptRequest.sender;
                            res[0] = JsonConvert.SerializeObject(errorResponse);
                            res[1] = null;
                            return res;
                        }
                    }
                    else
                    {
                        errorResponse.code = ACCEPT_FRIEND_REQUEST + ERROR_ADDER;
                        errorResponse.content = "you dont have a pending friend request from " + acceptRequest.sender;
                        res[0] = JsonConvert.SerializeObject(errorResponse);
                        res[1] = null;
                        return res;
                    }
                }
            }
            else
            {
                errorResponse.code = ACCEPT_FRIEND_REQUEST + ERROR_ADDER;
                errorResponse.content = "username does not exist";
                res[0] = JsonConvert.SerializeObject(errorResponse);
                res[1] = null;
                return res;
            }
        }

        public string[] SendFriendRequest(FriendRequest friendRequest, Dictionary<string, LinkedList<string>> friendsRequests)
        {
            Helper Helper = new Helper();
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            SQLHandler SQLHandler = new SQLHandler();

            string[] res = new string[2];
            res[1] = null;
            if (friendRequest.sender == friendRequest.recipent)
            {
                errorResponse.code = SEND_FRIEND_REQUEST + ERROR_ADDER;
                errorResponse.content = "cant send to yourself, how sad, you dont have friends so you requesting yourself.";
                res[0] = JsonConvert.SerializeObject(errorResponse);
                return res;
            }
            else
            {
                if (SQLHandler.checkIfUserExists(friendRequest.sender) && SQLHandler.checkIfUserExists(friendRequest.recipent))
                {
                    if (Helper.IfFriends(friendRequest.sender, friendRequest.recipent))
                    {
                        errorResponse.code = SEND_FRIEND_REQUEST + ERROR_ADDER;
                        errorResponse.content = "already friends!";
                        res[0] = JsonConvert.SerializeObject(errorResponse);
                        return res;
                    }
                    else
                    {
                        if (friendsRequests.ContainsKey(friendRequest.recipent))
                        {
                            if (friendsRequests[friendRequest.recipent].Find(friendRequest.sender) != null)
                            {
                                errorResponse.code = SEND_FRIEND_REQUEST + ERROR_ADDER;
                                errorResponse.content = "already sent friend request";
                                res[0] = JsonConvert.SerializeObject(errorResponse);
                                return res;
                            }
                            else
                            {
                                friendsRequests[friendRequest.recipent].AddLast(friendRequest.sender);
                                responseFormat.code = SEND_FRIEND_RESPONSE;
                                responseFormat.content = "request sent successfully!";
                                res[0] = JsonConvert.SerializeObject(responseFormat);
                                responseFormat.code = SEND_FRIEND_REQUEST_TO_CLIENT;
                                responseFormat.content = friendRequest.sender;
                                res[1] = JsonConvert.SerializeObject(responseFormat);
                                return res;
                            }
                        }
                        else
                        {
                            friendsRequests.Add(friendRequest.recipent, new LinkedList<string>());
                            friendsRequests[friendRequest.recipent].AddLast(friendRequest.sender);
                            responseFormat.code = SEND_FRIEND_RESPONSE;
                            responseFormat.content = "request sent successfully!";
                            res[0] = JsonConvert.SerializeObject(responseFormat);
                            responseFormat.code = SEND_FRIEND_REQUEST_TO_CLIENT;
                            responseFormat.content = friendRequest.sender;
                            res[1] = JsonConvert.SerializeObject(responseFormat);
                            return res;
                        }
                    }
                }
                else
                {
                    errorResponse.code = SEND_FRIEND_REQUEST + ERROR_ADDER;
                    errorResponse.content = "username does not exist";
                    res[0] = JsonConvert.SerializeObject(errorResponse);
                    return res;
                }
            }
        }
    }

}
