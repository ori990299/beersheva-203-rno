﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.IO;
using static System.Net.IPEndPoint;


namespace Server
{
    class RequestHandler
    {
        const int SIGNIN_REQUEST = 200;
        const int SIGNIN_RESPONSE = 201;
        const int SIGNUP_REQUEST = 100;
        const int SIGNUP_RESPONSE = 101;
        const int GET_FRIENDS_REQUEST = 110;
        const int GET_FRIENDS_RESPONSE = 111;
        const int SEND_FRIEND_REQUEST = 120;
        const int SEND_FRIEND_RESPONSE = 121;
        const int SEND_FRIEND_REQUEST_TO_CLIENT = 123;
        const int ACCEPT_FRIEND_REQUEST = 130;
        const int ACCEPT_FRIEND_RESPONSE = 131;
        const int DENY_FRIEND_REQUEST = 140;
        const int DENY_FRIEND_RESPONSE = 141;
        const int GET_ALL_FRIEND_REQUESTS = 150;
        const int GET_ALL_FRIEND_RESPONSE = 151;
        const int SEND_MESSAGE_REQUEST = 250;
        const int SEND_MESSAGE_RESPONSE = 251;
        const int ERROR_ADDER = 5;
        const int CLIENT_DISCONNECT_CODE = 8188;
        
        private Queue<Tuple<Socket, string>> clientsQueue;
        private Mutex queueMutex;
        private Dictionary<int, Socket> connectedUsers;
        private Dictionary<string, LinkedList<string>> friendsRequests = new Dictionary<string, LinkedList<string>>();
        private Helper Helper = new Helper();

        //Handlers
        private SQLHandler SQLHandler;
        private SignHandle SignHandle = new SignHandle();
        private FriendRequestsHandler FriendRequestsHandler = new FriendRequestsHandler();
        private ChatHandler ChatHandler = new ChatHandler();
        public RequestHandler()
        {
            this.queueMutex = new Mutex();
            this.clientsQueue = new Queue<Tuple<Socket, string>>();
            this.SQLHandler = new SQLHandler();
            this.connectedUsers = new Dictionary<int, Socket>();
        }

        public Dictionary<int, Socket> GetClients()
        {
            return this.connectedUsers;
        }

        private void Send(Socket handler, String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.UTF8.GetBytes(data);

            // Begin sending the data to the remote device.  
            handler.Send(byteData);
        }

        public void HandleRequest(Socket socket, string message)
        {
            Console.WriteLine("in handle request");
            MessageFormat messageformat = new MessageFormat(0, "");
            try
            {
                messageformat = JsonConvert.DeserializeObject<MessageFormat>(message);
            }
            catch(Exception e)
            {
                return;
            }
            ErrorResponse errorResponse = new ErrorResponse(0, "");
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            Console.WriteLine(messageformat.code);
            switch (messageformat.code)
            {
                case SIGNIN_REQUEST:
                    SigninFormat signinFormat = new SigninFormat("", "");
                    try
                    {
                        signinFormat = JsonConvert.DeserializeObject<SigninFormat>(messageformat.content);
                        
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                    Send(socket, SignHandle.SigninHandle(signinFormat, this.connectedUsers, socket));
                    break;
                case SIGNUP_REQUEST:
                    SignupFormat signupFormat = new SignupFormat("", "", "");
                    try
                    {
                        signupFormat = JsonConvert.DeserializeObject<SignupFormat>(messageformat.content);
                        
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                    Send(socket, SignHandle.SignUpHandle(signupFormat));
                    break;
                case GET_FRIENDS_REQUEST:
                    GetFriendFormat getFriendFormat = new GetFriendFormat("");
                    try
                    {
                        getFriendFormat = JsonConvert.DeserializeObject<GetFriendFormat>(messageformat.content);
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                    Send(socket, FriendRequestsHandler.GetFriends(getFriendFormat));
                    break;
                case SEND_FRIEND_REQUEST:
                    FriendRequest friendRequest = new FriendRequest("", "");
                    try
                    {
                        friendRequest = JsonConvert.DeserializeObject<FriendRequest>(messageformat.content);
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                    string[] Res = FriendRequestsHandler.SendFriendRequest(friendRequest, friendsRequests);
                    Send(socket, Res[0]);
                    if(Res[1] != null)
                    {
                        if (this.connectedUsers.ContainsKey(SQLHandler.GetUserId(friendRequest.recipent)))
                        {
                            Send(this.connectedUsers[SQLHandler.GetUserId(friendRequest.recipent)], Res[1]);
                        }
                    }
                    break;
                case ACCEPT_FRIEND_REQUEST:
                    FriendRequest acceptRequest = new FriendRequest("", "");
                    try
                    {
                        acceptRequest = JsonConvert.DeserializeObject<FriendRequest>(messageformat.content);
                    }
                    catch (Exception e)
                    {
                        break;
                    }
                    string[] res = FriendRequestsHandler.AcceptFriendRequest(acceptRequest, friendsRequests);
                    Send(socket, res[0]);
                    if (res[1] != null)
                    {
                        if (this.connectedUsers.ContainsKey(SQLHandler.GetUserId(acceptRequest.sender)))
                        {
                            Send(this.connectedUsers[SQLHandler.GetUserId(acceptRequest.sender)], res[1]);
                        }
                    }
                    break;
                case DENY_FRIEND_REQUEST:
                    FriendRequest denyRequest = new FriendRequest("", "");
                    try
                    {
                        denyRequest = JsonConvert.DeserializeObject<FriendRequest>(messageformat.content);
                    }
                    catch(Exception e)
                    {
                        break;
                    }
                    Send(socket, FriendRequestsHandler.DenyFriendRequest(denyRequest,friendsRequests));
                    break;
                case GET_ALL_FRIEND_REQUESTS:
                    GetAllFriendsRequests getAllFriendsRequests = new GetAllFriendsRequests("");
                    try
                    {
                        getAllFriendsRequests = JsonConvert.DeserializeObject<GetAllFriendsRequests>(messageformat.content);
                    }
                    catch(Exception e)
                    {
                        break;
                    }
                    Send(socket, FriendRequestsHandler.GetAllFriendsRequests(getAllFriendsRequests, this.friendsRequests));
                    break;
                case SEND_MESSAGE_REQUEST:
                    SendMessageFormat sendMessageFormat = new SendMessageFormat("", "", "", "");
                    try
                    {
                        sendMessageFormat = JsonConvert.DeserializeObject<SendMessageFormat>(messageformat.content);
                    }
                    catch(Exception e)
                    {
                        break;
                    }
                    string[] values = ChatHandler.SendMessageHandler(sendMessageFormat);
                    Send(socket, values[0]);
                    if (values[1] != null)
                    {
                        if (this.connectedUsers.ContainsKey(SQLHandler.GetUserId(sendMessageFormat.recipent)))
                        {
                            Send(this.connectedUsers[SQLHandler.GetUserId(sendMessageFormat.recipent)], values[1]);
                        }
                    }
                    break;
            }

        }
    }
}
