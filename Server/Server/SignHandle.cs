﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;


namespace Server
{

    class SignHandle
    {
        const int SIGNIN_REQUEST = 200;
        const int SIGNIN_RESPONSE = 201;
        const int SIGNUP_REQUEST = 100;
        const int SIGNUP_RESPONSE = 101;
        const int ERROR_ADDER = 5;

        private ErrorResponse errorResponse;
        private SQLHandler SQLHandler;
        public SignHandle()
        {
            this.errorResponse = new ErrorResponse(0,"");
            this.SQLHandler = new SQLHandler();

        }
        public string SigninHandle(SigninFormat signinFormat, Dictionary<int, Socket> connectedUsers, Socket socket)
        {
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            int userId = SQLHandler.checkMatchPassword(signinFormat.username, signinFormat.password);
            if (userId != -1)
            {
                if (connectedUsers.ContainsKey(userId))
                {
                    errorResponse.code = SIGNIN_REQUEST + ERROR_ADDER;
                    errorResponse.content = "user is already connected!";
                    return JsonConvert.SerializeObject(errorResponse);
                }
                else
                {
                    connectedUsers.Add(userId, socket);
                    responseFormat.code = SIGNIN_RESPONSE;
                    responseFormat.content = "user logged in successfully";
                    return JsonConvert.SerializeObject(responseFormat);
                }
            }
            else
            {
                errorResponse.code = SIGNIN_REQUEST + ERROR_ADDER;
                errorResponse.content = "username/password doesnt match!";
                return JsonConvert.SerializeObject(errorResponse);
            }
        }

        public string SignUpHandle(SignupFormat signupFormat)
        {
            Helper Helper = new Helper();
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            if (Helper.CheckUsernameValid(signupFormat.username) && Helper.CheckPasswordValid(signupFormat.password) && Helper.IsValidEmail(signupFormat.mail))
            {
                if (SQLHandler.checkIfUserExists(signupFormat.username) == false)
                {
                    if (SQLHandler.SignUpNewUser(signupFormat.username, signupFormat.password, signupFormat.mail))
                    {
                        responseFormat.code = SIGNUP_RESPONSE;
                        responseFormat.content = "user signed up successfully";
                        return JsonConvert.SerializeObject(responseFormat);
                    }
                    else
                    {
                        errorResponse.code = SIGNUP_REQUEST + ERROR_ADDER;
                        errorResponse.content = "something went wrong!";
                        return JsonConvert.SerializeObject(errorResponse);
                    }
                }
                else
                {
                    errorResponse.code = SIGNUP_REQUEST + ERROR_ADDER;
                    errorResponse.content = "username already exists!";
                    return JsonConvert.SerializeObject(errorResponse);
                }
            }
            else
            {
                errorResponse.code = SIGNUP_REQUEST + ERROR_ADDER;
                errorResponse.content = "follow the rules of the signup!";
                return JsonConvert.SerializeObject(errorResponse);
            }
        }
    }
}
