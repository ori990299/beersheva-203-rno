﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace Server
{
    class ChatHandler
    {
        const int SEND_MESSAGE_REQUEST = 250;
        const int SEND_MESSAGE_RESPONSE = 251;
        const int SEND_MESSAGE_TO_CLIENT = 253;
        const int ERROR_ADDER = 5;
        public string[] SendMessageHandler(SendMessageFormat sendChatFormat)
        {
            SQLHandler sQLHandler = new SQLHandler();
            ErrorResponse errorResponse = new ErrorResponse(0, "");
            ResponseFormat responseFormat = new ResponseFormat(0, "");
            string[] res = new string[2];
            res[1] = null;
            if(sQLHandler.checkIfUserExists(sendChatFormat.sender) && sQLHandler.checkIfUserExists(sendChatFormat.recipent))
            {
                if(ValidateMessage(sendChatFormat.message))
                {
                    SendBackToSenderFormat sendBackToSenderFormat = new SendBackToSenderFormat(sendChatFormat.recipent, sendChatFormat.guid);
                    responseFormat.code = SEND_MESSAGE_RESPONSE;
                    responseFormat.content = JsonConvert.SerializeObject(sendBackToSenderFormat);
                    res[0] = JsonConvert.SerializeObject(responseFormat);
                    SendMessageToClient sendMessageToClient = new SendMessageToClient(sendChatFormat.sender, sendChatFormat.message);
                    responseFormat.code = SEND_MESSAGE_TO_CLIENT;
                    responseFormat.content = JsonConvert.SerializeObject(sendMessageToClient);
                    res[1] = JsonConvert.SerializeObject(responseFormat);
                    return res;
                }
                else
                {
                    errorResponse.code = SEND_MESSAGE_REQUEST + ERROR_ADDER;
                    errorResponse.content = "u need message content!";
                    res[0] = JsonConvert.SerializeObject(errorResponse);
                    return res;
                }
            }
            else
            {
                errorResponse.code = SEND_MESSAGE_REQUEST + ERROR_ADDER;
                errorResponse.content = "username doesnt exist";
                res[0] = JsonConvert.SerializeObject(errorResponse);
                return res;
            }
        }

        private bool ValidateMessage(string msg)
        {
            if(msg.Length > 0)
            {
                if (IfOnlyOneCharachter(msg, ' '))
                {
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IfOnlyOneCharachter(string msg, char c)
        {
            for (int i = 0; i < msg.Length; i++)
            {
                if(msg[i] != c)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
