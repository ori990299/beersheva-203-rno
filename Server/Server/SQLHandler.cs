﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Server
{
    class SQLHandler
    {
        
        private SqlConnection conn = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=" + Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).ToString()).ToString() + @"\Database.mdf" + ";Integrated Security = True");
        public bool checkIfUserExists(string username)
        {
            string query = "SELECT * FROM users WHERE username='" + username + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if(dr.HasRows)
            {
                conn.Close();
                return true;
            }
            else
            {
                conn.Close();
                return false;
            }

        }
        public bool SignUpNewUser(string name, string password, string mail)
        {
            string query = "INSERT INTO users (username, password, mail) VALUES ('" + name + "','" + password + "','" + mail +"');";
            SqlCommand cmd = new SqlCommand(query, conn);
            
            conn.Open();
            int row = cmd.ExecuteNonQuery();
            if (row > 0)
            {
                conn.Close();
                query = "INSERT INTO friends (userName, friendsNames) VALUES ('" + name + "','" + null + "');";
                cmd = new SqlCommand(query, conn);
                conn.Open();
                row = cmd.ExecuteNonQuery();
                if (row > 0)
                {
                    conn.Close();
                    return true;
                }
                else
                {
                    conn.Close();
                    return false;
                }
            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public int checkMatchPassword(string username, string password)
        {
            string query = "SELECT PASSWORD, userId FROM users WHERE USERNAME = '" + username + "';";
            string DBpassword = "";
            int id = 0;
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    DBpassword = dr.GetString(0);
                    id = dr.GetInt32(1);
                    if (password == DBpassword)
                    {
                        conn.Close();
                        return id;

                    }
                    else
                    {
                        conn.Close();
                        return -1;
                    }
                }
                return -1;
            }
            else
            {
                conn.Close();
                return -1;
            }

        }
        public string GetFriends(string username)
        {
            string query = "SELECT friendsNames FROM friends WHERE userName = '" + username + "'";
            string names = "";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    names = dr.GetString(0);
                    conn.Close();
                    return names;
                }
                conn.Close();
                return "";
            }
            else
            {
                conn.Close();
                return "";
            }
        }
        public bool AddFriend(string username, string newFriend)
        {
            string friends = GetFriends(username);
            friends += newFriend;
            friends += ",";
            string query = "UPDATE friends SET friendsNames = '" + friends + "' WHERE userName = '" + username +"';";
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            int row = cmd.ExecuteNonQuery();

            if (row > 0)
            {
                conn.Close();
                return true;
            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public int GetUserId(string username)
        {
            string query = "SELECT userId FROM users WHERE USERNAME = '" + username + "';";
            int id = 0;
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    id = dr.GetInt32(0);
                    conn.Close();
                    return id;
                }
                conn.Close();
                return -1;
            }
            else
            {
                conn.Close();
                return -1;
            }
        }
    }
}
