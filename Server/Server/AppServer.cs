﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class AppServer
    {
        private Communicator communicator;
        private int numThreads;
        private int port;

        public AppServer(int numThreads, int port)
        {
            this.numThreads = numThreads;
            this.port = port;
        }
        public void run()
        {
            Thread connector = new Thread(new ThreadStart(callBindAndListen));
            connector.Start();
        }
        public void callBindAndListen()
        {
            try
            {
                this.communicator = new Communicator(this.port,this.numThreads);
                this.communicator.StartListening();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
