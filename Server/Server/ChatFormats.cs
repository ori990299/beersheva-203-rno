﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class SendMessageFormat
    {
        public string sender { get; set; }
        public string recipent { get; set; }
        public string message { get; set; }
        public string guid { get; set; }
        public SendMessageFormat(string sender, string recipent, string message, string guid)
        {
            this.sender = sender;
            this.recipent = recipent;
            this.message = message;
            this.guid = guid;
        }
    }

    class SendBackToSenderFormat
    {
        public string recipent { get; set; }
        public string guid { get; set; }
        public SendBackToSenderFormat(string recipent, string guid)
        {
            this.recipent = recipent;
            this.guid = guid;
        }
    }

    class SendMessageToClient
    {
        public string sender { get; set; }
        public string message { get; set; }
        public SendMessageToClient(string sender, string message)
        {
            this.sender = sender;
            this.message = message;
        }
    }
}
