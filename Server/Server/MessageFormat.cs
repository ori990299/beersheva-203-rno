﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class MessageFormat
    {
        public int code { get; set; }
        public string content { get; set; }

        public MessageFormat(int code, string content)
        {
            this.code = code;
            this.content = content;
        }
    }
    class ErrorResponse
    {
        public int code { get; set; }
        public string content { get; set; }

        public ErrorResponse(int code, string content)
        {
            this.code = code;
            this.content = content;
        }
    }

    class ResponseFormat
    {
        public int code { get; set; }
        public string content { get; set; }

        public ResponseFormat(int code, string content)
        {
            this.code = code;
            this.content = content;
        }
    }
}
