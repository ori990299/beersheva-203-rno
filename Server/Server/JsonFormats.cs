﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class SignupFormat
    {
        public string username { get; set; }
        public string password { get; set; }
        public string mail { get; set; }

        public SignupFormat(string username, string password, string mail)
        {
            this.username = username;
            this.password = password;
            this.mail = mail;
        }
    }
    class SigninFormat
    {
        public string username { get; set; }
        public string password { get; set; }

        public SigninFormat(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }

    class GetFriendFormat
    {
        public string username { get; set; }
        public GetFriendFormat(string username)
        {
            this.username = username;
        }
    }

    class FriendRequest
    {
        public string sender { get; set; }
        public string recipent { get; set; }
        public FriendRequest(string sender, string recipent)
        {
            this.sender = sender;
            this.recipent = recipent;
        }
    }

    class GetAllFriendsRequests
    {
        public string username { get; set; }
        public GetAllFriendsRequests(string username)
        {
            this.username = username;
        }
    }
}
