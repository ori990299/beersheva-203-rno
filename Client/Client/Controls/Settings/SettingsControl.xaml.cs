﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        public SettingsControl()
        {
            InitializeComponent();
        }

        private void ExitSeettingsButton_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.Properties["MainWindow"]).settingsGrid.Visibility = Visibility.Hidden;
        }

        private void inputDeviceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataSettings.Instance.changeSelectedInputDevice(this.inputDeviceComboBox.SelectedIndex);
        }
    }
}
