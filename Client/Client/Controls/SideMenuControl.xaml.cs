﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for SideMenuControl.xaml
    /// </summary>
    public partial class SideMenuControl : UserControl
    {
        public ChatListControl chatListControl { get; set; }

        public FriendListControl FriendListControl { get; set; }

        public SideMenuControl()
        {
            this.chatListControl = new ChatListControl();
            this.FriendListControl = new FriendListControl();

            this.Content = chatListControl;

            InitializeComponent();
        }

        public void ChatsButton_Click(object sender, RoutedEventArgs e)
        {
            ChatClick();
        }

        public void FriendsButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = FriendListControl;
        }

        public void BellButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = "No Notifications UI yet";
        }

        public void Settings_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.Properties["MainWindow"]).settingsGrid.Visibility = Visibility.Visible;
        }

        public void ChatClick()
        {
            this.Content = chatListControl;
        }
    }
}
