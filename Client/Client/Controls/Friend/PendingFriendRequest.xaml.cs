﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for PendingFriendRequest.xaml
    /// </summary>
    public partial class PendingFriendRequest : UserControl
    {
        public PendingFriendRequest()
        {
            InitializeComponent();
        }

        private void FriendAccept_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine($"You want to accept {this.senderName.Content}'s friend request.");
            Communicator.Instance.sendAcceptFriendRequest(this.senderName.Content.ToString());
        }

        private void FriendDeny_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine($"You want to deny {this.senderName.Content}'s friend request.");
            Communicator.Instance.sendDenyFriendRequest(this.senderName.Content.ToString());
        }
    }
}
