﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for FriendListItemControl.xaml
    /// </summary>
    public partial class FriendListItemControl : UserControl
    {
        public FriendListItemControl()
        {
            InitializeComponent();
        }

        private void name_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DataChat.Instance.addChat(FriendName.Content.ToString());

            DataChat.Instance.DisplayedChat = DataChat.Instance.getChatByName(FriendName.Content.ToString()).ShowChatPage;

            DataChat.Instance.cleanSelected();

            DataChat.Instance.getChatByName(FriendName.Content.ToString()).IsISelected = true;

            DataChat.Instance.getChatByName(FriendName.Content.ToString()).NewContentAvailable = false;

            ((MainWindow)App.Current.Properties["MainWindow"]).sideMenu.ChatClick();
        }
    }
}
