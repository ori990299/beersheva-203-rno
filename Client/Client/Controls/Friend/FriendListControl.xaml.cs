﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for FriendListControl.xaml
    /// </summary>
    public partial class FriendListControl : UserControl
    {
        public FriendListControl()
        {
            InitializeComponent();

            TextBlock tb = (TextBlock)this.friendListButton.Content;
            tb.Background = Brushes.White;

            this.friendListPanel.Visibility = Visibility.Visible;

            this.addFriendPanel.Visibility = Visibility.Collapsed;

            this.friendRequestMessage.Visibility = Visibility.Collapsed;
        }

        private void friendList_Click(object sender, RoutedEventArgs e)
        {
            TextBlock tb = (TextBlock)this.friendListButton.Content;
            tb.Background = Brushes.White;

            TextBlock tb2 = (TextBlock)this.addFriendsButton.Content;
            tb2.Background = Brushes.Transparent;

            this.friendListPanel.Visibility = Visibility.Visible;

            this.addFriendPanel.Visibility = Visibility.Collapsed;
        }

        private void addFriends_Click(object sender, RoutedEventArgs e)
        {
            TextBlock tb = (TextBlock)this.addFriendsButton.Content;
            tb.Background = Brushes.White;

            TextBlock tb2 = (TextBlock)this.friendListButton.Content;
            tb2.Background = Brushes.Transparent;

            AddFriendViewModel.Instance.Message = "Loading...";
            AddFriendViewModel.Instance.ColorRGB = "000000";

            this.addFriendPanel.Visibility = Visibility.Visible;

            this.friendListPanel.Visibility = Visibility.Collapsed;

            this.friendRequestMessage.Visibility = Visibility.Collapsed;
        }

        private void sendFriendRequest_Click(object sender, RoutedEventArgs e)
        {
            Communicator.Instance.sendFriendRequest(this.recipientUsername.Text.ToString());
            this.friendRequestMessage.Visibility = Visibility.Visible;
        }

        private void recipientUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                sendFriendRequest_Click(sender,e);
            }
        }
    }
}
