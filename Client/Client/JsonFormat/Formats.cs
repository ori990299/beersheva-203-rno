﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class MessageFormat
    {
        public int code { get; set; }
        public string content { get; set; }

        public MessageFormat(int code, string content)
        {
            this.code = code;
            this.content = content;
        }
    }

    class SendMessageResponse
    {
        public string recipent { get; set; }
        public string guid { get; set; }

        public SendMessageResponse()
        {

        }
    }

    class GotMessageFormat
    {
        public string sender { get; set; }
        public string message { get; set; }

        public GotMessageFormat()
        {

        }
    }

    class SendLogIn
    {
        public string username { get; set; }
        public string password { get; set; }

        public SendLogIn()
        {
                
        }
    }

    class SendGetAllFriends
    {
        public string username { get; set; }

        public SendGetAllFriends()
        {

        }
    }

    class SendGetAllFriendRequest
    {
        public string username { get; set; }

        public SendGetAllFriendRequest()
        {

        }
    }

    class SendFriendRequest
    {
        public string sender { get; set; }
        public string recipent { get; set; }

        public SendFriendRequest()
        {

        }
    }

    class SendAcceptFriendRequest
    {
        public string sender { get; set; }
        public string recipent { get; set; }

        public SendAcceptFriendRequest()
        {

        }
    }

    class SendDenyFriendRequest
    {
        public string sender { get; set; }
        public string recipent { get; set; }

        public SendDenyFriendRequest()
        {

        }
    }

    class SendMessageRequest
    {
        public string sender { get; set; }
        public string recipent { get; set; }
        public string message { get; set; }
        public string guid { get; set; }

        public SendMessageRequest()
        {

        }
    }
}
