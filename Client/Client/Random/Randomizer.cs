﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{

    class Randomizer
    {
        /// <summary>
        /// Instance
        /// </summary>

        private static Randomizer instance = null;

        private static readonly object padlock = new object();
        public static Randomizer Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Randomizer();
                    }
                    return instance;
                }
            }
        }


        public Random random { get; set; }

        /// <summary>
        /// The RGB values (in hex) for the backgroundsColor List
        /// Fore exmeple FF00FF Red and Blue mixed
        ///  colors picker > https://www.w3schools.com/colors/colors_picker.asp
        /// </summary>
        private List<string> backgroundsColor { get; set; }

        private Randomizer()
        {
            this.random = new Random();

            this.backgroundsColor = new List<string>
            {
                "02c1e3", //Cyan
                "fce803", //Yellow
                "c91af0", //purple pink
                "02e3a0", //Mountain Meadow color(green)
                "028af2", //Blue
                "ff751a", //Orange
                "ff3333", //Red
                "884dff", //violet
            };
        }

        public string hexColorForUserBacground()
        {
            return backgroundsColor[random.Next(backgroundsColor.Count)];
        }

    }
}
