﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace Client
{
    class DataCenter : BaseViewModel
    {
        #region CONSTS
        
        const int GET_FRIENDS_RESPONSE = 111;
        const int SEND_FRIEND_REQUEST_RESPONSE = 121;
        const int GET_ALL_FRIEND_REQUESTS_RESPONSE = 151;
        const int ACCEPT_FRIEND_RESPONSE = 131;
        const int DENY_FRIEND_RESPONSE = 141;
        const int SEND_MESSAGE_RESPONSE = 251;

        #endregion

        /// <summary>
        /// Instance
        /// </summary>

        private static DataCenter instance = null;

        private static readonly object padlock = new object();
        public static DataCenter Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DataCenter();
                    }
                    return instance;
                }
            }
        }

        /// <summary>
        /// Data
        /// </summary>
        private ObservableCollectionEx<FriendListItemViewModel>  _friendList;
        public ObservableCollectionEx<FriendListItemViewModel> FriendList
        {
            get
            {
                return _friendList;
            }
            set
            {
                _friendList = value;
                _friendList.CollectionChanged += FriendList_CollectionChanged;
                OnPropertyChanged("FriendList");
            }
        }

        private DataCenter()
        {
            this.FriendList = new ObservableCollectionEx<FriendListItemViewModel>(new List<FriendListItemViewModel>());
        }

        private void FriendList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("FriendList");
        }

        public void updateData(MessageFormat messageformat)
        {
            if (messageformat.code == GET_FRIENDS_RESPONSE)
            {
                setFriendList(messageformat);
            }
            else if(messageformat.code == SEND_FRIEND_REQUEST_RESPONSE)
            {
                setFriendRequestResponseMessage(messageformat);
            }
            else if(messageformat.code == GET_ALL_FRIEND_REQUESTS_RESPONSE)
            {
                DataNotification.Instance.setFriendRequestToList(messageformat);
            }
            else if(messageformat.code == ACCEPT_FRIEND_RESPONSE)
            {
                Communicator.Instance.sendGetAllFriends();
                DataNotification.Instance.removeFriendRequestToList(messageformat);
            }
            else if (messageformat.code == DENY_FRIEND_RESPONSE)
            {
                DataNotification.Instance.removeFriendRequestToList(messageformat);
            }
            else if (messageformat.code == SEND_MESSAGE_RESPONSE)
            {
                DataChat.Instance.setMyMessageToBlack(messageformat);
            }
        }

        private void setFriendList(MessageFormat message)
        {
            string[] friends = message.content.Split(',');

            this.FriendList.Clear();

            for (int i = 0; i < friends.Length - 1; i++)
            {
                this.FriendList.Add(
                    new FriendListItemViewModel
                    {
                        Name = friends[i],
                        Initials = friends[i][0].ToString().ToUpper(),
                        ProfilePictureRGB = Randomizer.Instance.hexColorForUserBacground(),
                    });
            }
        }

        private void setFriendRequestResponseMessage(MessageFormat message)
        {
            AddFriendViewModel.Instance.Message = message.content;
            AddFriendViewModel.Instance.ColorRGB = "47d147";
        }

    }
}
