﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Client
{
    class DataHandler
    {

        #region CONSTS

        const int RESPONSE = 1;
        const int ERROR = 5;
        const int NOTIFICATIONS = 3;

        #endregion

        /// <summary>
        /// Instance
        /// </summary>

        private static DataHandler instance = null;

        private static readonly object padlock = new object();
        public static DataHandler Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DataHandler();
                    }
                    return instance;
                }
            }
        }

        private DataHandler()
        {

        }

         public void handleMessageToData(string message)
        {
            MessageFormat messageformat;

            try
            {
                messageformat = JsonConvert.DeserializeObject<MessageFormat>(message);
            }
            catch (Exception)
            {

                Console.WriteLine("Got message from server but not in the MessageFormat. \nMessage: \n" + message);
                return;
            }

            int messageType = messageformat.code % 10; // the '% 10' is so we can get the most right number int the message aka message type

            if (messageType == RESPONSE)
            {
                DataCenter.Instance.updateData(messageformat);
            }
            else if(messageType == ERROR)
            {
                DataError.Instance.updateData(messageformat);
            }
            else if (messageType == NOTIFICATIONS)
            {
                DataNotification.Instance.updateData(messageformat);
            }

        }
    }
}
