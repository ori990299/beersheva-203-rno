﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows;
using System.Windows.Controls;

namespace Client
{
    class DataError : BaseViewModel
    {
        #region CONSTS

        const int GET_FRIENDS_ERROR = 115;
        const int SEND_FRIEND_REQUEST_ERROR = 125;
        const int GET_ALL_FRIEND_REQUESTS_ERROR = 155;
        const int ACCEPT_FRIEND_ERROR = 135;
        const int DENY_FRIEND_ERRORE = 145;
        const int SEND_MESSAGE_ERRORE = 255;

        #endregion

        /// <summary>
        /// Instance
        /// </summary>

        private static DataError instance = null;

        private static readonly object padlock = new object();
        public static DataError Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DataError();
                    }
                    return instance;
                }
            }
        }

        /// <summary>
        /// Data
        /// </summary>

        private DataError()
        {

        }

        public void updateData(MessageFormat messageformat)
        {
            if (messageformat.code == GET_FRIENDS_ERROR)
            {
                //pass
            }
            else if(messageformat.code == SEND_FRIEND_REQUEST_ERROR)
            {
                setFriendRequestErrorMessage(messageformat);
            }
            else if (messageformat.code == GET_ALL_FRIEND_REQUESTS_ERROR)
            {
                //pass
            }
            else if (messageformat.code == ACCEPT_FRIEND_ERROR)
            {
                //pass
            }
            else if (messageformat.code == DENY_FRIEND_ERRORE)
            {
                //pass
            }
            else if (messageformat.code == SEND_MESSAGE_ERRORE)
            {
                DataChat.Instance.setMyMessageToRed(messageformat);
            }
        }

        private void setFriendRequestErrorMessage(MessageFormat message)
        {
            AddFriendViewModel.Instance.Message = message.content;
            AddFriendViewModel.Instance.ColorRGB = "ff3300";
        }
    }
}
