﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class DataSettings : BaseViewModel
    {
        /// <summary>
        /// Instance
        /// </summary>
        private static DataSettings instance = null;

        private static readonly object padlock = new object();
        public static DataSettings Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DataSettings();
                    }
                    return instance;
                }
            }
            set { instance = value; }

        }

        /// <summary>
        /// Data
        /// </summary>
        public ObservableCollectionEx<string> InputDevices { get; set; }
        public ObservableCollectionEx<string> OutputDevices { get; set; }
        public string SelectedInputDevice { get; set; }
        public int SelectedInputDeviceNumber { get; set; }

        private DataSettings()
        {
            InputDevices = new ObservableCollectionEx<string>(new List<string>());

            SelectedInputDevice = WaveIn.GetCapabilities(0).ProductName;// sets the default wavein
            SelectedInputDeviceNumber = 0;

            getAllInputDevicesFromMachine();
        }

        private void getAllInputDevicesFromMachine()
        {
            for (int i = 0; i < WaveIn.DeviceCount; i++)// gets all the devices + print them all out
            {
                var device = WaveIn.GetCapabilities(i);
                InputDevices.Add(device.ProductName);
            }
        }

        public void changeSelectedInputDevice(int numberOfSelectedDevice)
        {
            this.SelectedInputDeviceNumber = numberOfSelectedDevice;
            Voip.Instance.changeInputVOIP(numberOfSelectedDevice);
        }
    }
}
