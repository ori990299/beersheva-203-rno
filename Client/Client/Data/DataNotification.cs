﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace Client
{
    class DataNotification : BaseViewModel
    {
        #region CONSTS

        const int GOT_FRIEND_REQUEST_NOTIFICATION = 123;
        const int GOT_MESSAGE = 253;

        #endregion

        /// <summary>
        /// Instance
        /// </summary>
        private static DataNotification instance = null;

        private static readonly object padlock = new object();
        public static DataNotification Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DataNotification();
                    }
                    return instance;
                }
            }
        }

        /// <summary>
        /// Data
        /// </summary>
        private ObservableCollectionEx<PendingFriendRequestViewModel> _pendingfriendrequest;
        public ObservableCollectionEx<PendingFriendRequestViewModel> PendingFriendRequest
        {
            get
            {
                return _pendingfriendrequest;
            }
            set
            {
                _pendingfriendrequest = value;
                _pendingfriendrequest.CollectionChanged += PendingFriendRequest_CollectionChanged;
                OnPropertyChanged("PendingFriendRequest");
            }
        }

        private DataNotification()
        {
            this.PendingFriendRequest = new ObservableCollectionEx<PendingFriendRequestViewModel>(new List<PendingFriendRequestViewModel>());
        }

        private void PendingFriendRequest_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("PendingFriendRequest");
        }

        public void updateData(MessageFormat messageformat)
        {
            if (messageformat.code == GOT_FRIEND_REQUEST_NOTIFICATION)
            {
                addFriendRequestToList(messageformat);
            }
            else if (messageformat.code == GOT_MESSAGE)
            {
                GotMessageFormat messageFromSender = JsonConvert.DeserializeObject<GotMessageFormat>(messageformat.content);

                DataChat.Instance.addChat(messageFromSender.sender);
                

                DataChat.Instance.addMessageFromSender(messageFromSender.sender, messageFromSender.message);
            }

        }

        public void setFriendRequestToList(MessageFormat message)
        {
            string[] friends = message.content.Split(',');

            for (int i = 0; i < friends.Length - 1; i++)
            {
                this.PendingFriendRequest.Add(
                    new PendingFriendRequestViewModel
                    {
                        Name = friends[i],
                        Initials = friends[i][0].ToString().ToUpper(),
                        ProfilePictureRGB = Randomizer.Instance.hexColorForUserBacground(),
                    });
            }
        }

        public void removeFriendRequestToList(MessageFormat message)
        {
            string name = message.content;

            foreach (PendingFriendRequestViewModel PendingRequest in this.PendingFriendRequest)
            {
                if (PendingRequest.Name == message.content)
                {
                    this.PendingFriendRequest.Remove(PendingRequest);
                    return;
                }
            }
        }

        private void addFriendRequestToList(MessageFormat message)
        {
            string name = message.content;

            this.PendingFriendRequest.Add(
                new PendingFriendRequestViewModel
                {
                    Name = name,
                    Initials = name[0].ToString().ToUpper(),
                    ProfilePictureRGB = Randomizer.Instance.hexColorForUserBacground(),
                });
        }
    }
}
