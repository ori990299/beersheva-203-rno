﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class DataChat : BaseViewModel
    {

        /// <summary>
        /// Instance
        /// </summary>
        private static DataChat instance = null;

        private static readonly object padlock = new object();
        public static DataChat Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DataChat();
                    }
                    return instance;
                }
            }
            set { instance = value; }
            
        }

        /// <summary>
        /// Data
        /// </summary>
        private ObservableCollectionEx<ChatListItemViewModel> _chatlist;
        public ObservableCollectionEx<ChatListItemViewModel> ChatList
        {
            get
            {
                return _chatlist;
            }
            set
            {
                _chatlist = value;
                _chatlist.CollectionChanged += ChatList_CollectionChanged;
                OnPropertyChanged("ChatList");
            }
        }
        private void ChatList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("ChatList");
        }

        private ChatPage _displayedchat { get; set; }
        public ChatPage DisplayedChat
        {
            get
            {
                return _displayedchat;
            }
            set
            {
                _displayedchat = value;
                OnPropertyChanged("DisplayedChat");
            }
        }
        private Dictionary<string, ChatMessageViewModel> MessagesFromMe { get; set; }

        public DataChat()
        {
            this.DisplayedChat = null;
            this.ChatList = new ObservableCollectionEx<ChatListItemViewModel>(new List<ChatListItemViewModel>());
            this.MessagesFromMe = new Dictionary<string, ChatMessageViewModel>();
        }

        public void cleanSelected()
        {
            foreach (ChatListItemViewModel chat in this.ChatList)
            {
                if (chat.IsISelected == true)
                {
                    chat.IsISelected = false;
                    return;
                }
            }
            return;
        }

        public void addChat(string name)
        {
            var isInlist = ChatList.Where(a => a.Name == name).FirstOrDefault();

            if (isInlist == null)
            {
               var x = new ChatListItemViewModel(name);
               x.ProfilePictureRGB = DataCenter.Instance.FriendList.Where(a => a.Name == name).FirstOrDefault().ProfilePictureRGB;
               this.ChatList.Add(x);
            }
        }

        private void moveToTop(ChatListItemViewModel chat)
        {
            this.ChatList.Move(this.ChatList.IndexOf(chat), 0);
            return;
        }

        public ChatListItemViewModel getChatByName(string chatName)
        {
            foreach (ChatListItemViewModel chat in this.ChatList)
            {
                if (chat.Name == chatName)
                {
                    return chat;
                }
            }

            return null;
        }

        private void updateLastMessage(ChatListItemViewModel chat, string message)
        {
            chat.Message = message;
        }

        public void addMessageFromSender(string chatName, string message)
        {
            ChatListItemViewModel chat = getChatByName(chatName);
            if (chat != null)
            {
                if (chat.IsISelected == false)
                {
                    chat.NewContentAvailable = true;
                }

                updateLastMessage(chat, message);

                moveToTop(chat);

                chat.ChatMessages.Add(new ChatMessageViewModel
                {
                    Name = chat.Name,
                    Initials = chat.Initials,
                    Message = message,
                    ProfilePictureRGB = chat.ProfilePictureRGB,
                    MessageFontRGB = "#000000",
                });

                SoundPlayer sp = new SoundPlayer(Client.Properties.Resources.New_Message);
                sp.Play();

            }

            return;
        }

        public void addMessageFromMe(string chatName, string message, string guid)
        {
            ChatListItemViewModel chat = getChatByName(chatName);
            if (chat != null)
            {
                updateLastMessage(chat, message);

                moveToTop(chat);

                var messageInChat = new ChatMessageViewModel
                {
                    Name = (string)App.Current.Properties["username"],
                    Initials = (string)App.Current.Properties["initials"],
                    Message = message,
                    ProfilePictureRGB = (string)App.Current.Properties["profilepicturergb"],
                    MessageFontRGB = "#858585",
                };

                chat.ChatMessages.Add(messageInChat);

                this.MessagesFromMe.Add(guid, messageInChat);

            }
            
            return;
        }

        public void setMyMessageToBlack(MessageFormat messageformat)
        {
            SendMessageResponse messageResponse = JsonConvert.DeserializeObject<SendMessageResponse>(messageformat.content);

            this.MessagesFromMe[messageResponse.guid].MessageFontRGB = "#000000";
        }

        public void setMyMessageToRed(MessageFormat messageformat)
        {
            
            SendMessageResponse messageResponse = JsonConvert.DeserializeObject<SendMessageResponse>(messageformat.content);

            this.MessagesFromMe[messageResponse.guid].MessageFontRGB = "#ff1500";
        }
    }
}
