﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class DataUser
    {
        /// <summary>
        /// Instance
        /// </summary>
        private static DataUser instance = null;

        private static readonly object padlock = new object();
        public static DataUser Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new DataUser();
                    }
                    return instance;
                }
            }
            set { instance = value; }

        }

        public string Name { get; set; }
        public string Initials { get; set; }
        public string ProfilePictureRGB { get; set; }

        public DataUser()
        {

        }
    }
}
