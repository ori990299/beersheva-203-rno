﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Threading;

namespace Client
{
    /// <summary>
    /// The State Object for the async Recive
    /// </summary>
    public class StateObject
    {
        // Size of receive buffer.  
        public const int BufferSize = 1024;

        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];

        // Received data string.
        public StringBuilder sb = new StringBuilder();

        // Client socket.
        public Socket workSocket = null;
    }

    public class Communicator
    {

        #region server configs

        const string SERVER_IP = "176.230.211.127";
        const int PORT = 6666;

        #endregion

        #region CONSTS

        const int GET_FRIENDS = 110;
        const int SEND_FRIEND_REQUEST = 120;
        const int GET_ALL_FRIEND_REQUESTS = 150;
        const int ACCEPT_FRIEND_REQUEST = 130;
        const int DENY_FRIEND_REQUEST = 140;
        const int SEND_MESSAGE_REQUEST = 250;
        const int SEND_LOGIN_REQUEST = 200;

        #endregion

        /// <summary>
        /// Server Connection properties: IP, Port, Socket
        /// </summary>
        public TcpClient client { get; set; }
        public string serverIP { get; set; }
        public int port { get; set; }

        /// <summary>
        /// Instance
        /// </summary>

        private static Communicator instance = null;

        private static readonly object padlock = new object();
        public static Communicator Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Communicator(SERVER_IP,PORT);
                    }
                    return instance;
                }
            }
        }

        private Communicator(string serverIP, int port)
        {
            //Server Connection properties: IP, Port, Socket
            this.serverIP = serverIP;
            this.port = port;

            try//try and connect the server
            {
                this.client = new TcpClient(serverIP, port);
            }
            catch
            {
                this.client = null;
                Thread thread1 = new Thread(tryToConnect);
                thread1.Start();
            }
        }
        
        private void tryToConnect()
        {
            while (this.client == null)
            {
                try//try and connect the server
                {
                    this.client = new TcpClient(this.serverIP, this.port);
                }
                catch
                {
                    this.client = null;
                }
            }
        }

        public Boolean send(string msg)
        {
            var toSend = Encoding.UTF8.GetBytes(msg.ToString());

            if (this.client != null)
            {
                try
                {
                    NetworkStream stream = this.client.GetStream();
                    stream.Write(toSend, 0, toSend.Length);
                }
                catch
                {
                    System.Windows.MessageBox.Show("Can't send.", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                    return false;
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Can't connect the server.", "Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        public MessageFormat receive()
        {
            byte[] receivedData = new byte[1024];
            MessageFormat messageformat;

            try
            {
                NetworkStream stream = this.client.GetStream();
                stream.Read(receivedData, 0, receivedData.Length);
            }
            catch
            {
                return new MessageFormat(-1, "Can't read message from socket.");
            }

            string msg = Encoding.UTF8.GetString(receivedData, 0, receivedData.Length);

            try
            {
                messageformat = JsonConvert.DeserializeObject<MessageFormat>(msg);
            }
            catch (Exception)
            {

                messageformat = new MessageFormat(-1,"Unknown message recived.");
            }

            return messageformat;
        }

        public void StartAsyncRecive()
        {
            Socket handler = this.client.Client;

            StateObject state = new StateObject();
            state.workSocket = handler;

            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.
            int bytesRead = handler.EndReceive(ar);
            Console.WriteLine("after End recv" + bytesRead);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read
                // more data.  
                content = state.sb.ToString();

                //Console.WriteLine("got from socket:" + handler + " data:" + content);
                Console.WriteLine(content);

                DataHandler.Instance.handleMessageToData(content);

                // Echo the data back to the client.  
                state.sb.Clear();
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
            }
        }

        private string buildRequest(int code, string content)
        {
            MessageFormat message = new MessageFormat(code, content);

            string request = JsonConvert.SerializeObject(message);

            return request;
        }

        public MessageFormat sendLogIn(string username, string password)
        {
            string content = JsonConvert.SerializeObject(
                new SendLogIn
                {
                    username = username,
                    password = password
                });

            string request = buildRequest(SEND_LOGIN_REQUEST, content);

            bool isSended = send(request);

            if (isSended)
            {
                return receive();
            }
            else
            {
                return new MessageFormat(-1, "Can't send.");
            }
        }

        public bool sendGetAllFriends()
        {
            string content = JsonConvert.SerializeObject(
                new SendGetAllFriends
                {
                    username = App.Current.Properties["username"].ToString()
                });

            string request = buildRequest(GET_FRIENDS, content);

            return send(request);
        }

        public bool sendGetAllFriendRequest()
        {
            string content = JsonConvert.SerializeObject(
                new SendGetAllFriendRequest
                {
                    username = App.Current.Properties["username"].ToString()
                });

            string request = buildRequest(GET_ALL_FRIEND_REQUESTS, content);

            return send(request);
        }

        public bool sendFriendRequest(string recipientUsername)
        {
            string content = JsonConvert.SerializeObject(
                new SendFriendRequest
                {
                    sender = App.Current.Properties["username"].ToString(),
                    recipent = recipientUsername
                });

            string request = buildRequest(SEND_FRIEND_REQUEST, content);

            return send(request);
        }

        public bool sendAcceptFriendRequest(string sender)
        {
            string content = JsonConvert.SerializeObject(
                new SendAcceptFriendRequest
                {
                    sender = sender,
                    recipent = App.Current.Properties["username"].ToString()
                });

            string request = buildRequest(ACCEPT_FRIEND_REQUEST, content);

            return send(request);
        }

        public bool sendDenyFriendRequest(string sender)
        {
            string content = JsonConvert.SerializeObject(
                new SendDenyFriendRequest
                {
                    sender = sender,
                    recipent = App.Current.Properties["username"].ToString()
                });

            string request = buildRequest(DENY_FRIEND_REQUEST, content);

            return send(request);
        }

        public bool sendMessageRequest(string recipent, string message, string guid)
        {
            string content = JsonConvert.SerializeObject(
                new SendMessageRequest
                {
                    sender = App.Current.Properties["username"].ToString(),
                    recipent = recipent,
                    message = message,
                    guid = guid
                });

            string request = buildRequest(SEND_MESSAGE_REQUEST, content);

            return send(request);
        }
    }
}
