﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for SignInPage.xaml
    /// </summary>
    public partial class SignInPage : Page
    {
        public Boolean IsValidUsername { get; set; }
        public Boolean IsValidPassword { get; set; }
        public ClientWindow window { get; set; }

        public SignInPage(ClientWindow window)
        {
            this.IsValidUsername = false;
            this.IsValidPassword = false;
            this.window = window;

            InitializeComponent();

            this.loginButton.IsEnabled = false;
        }

        private void backgroundGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            signUpText.Focus();
        }

        private void checkUsernameAndPasswordInput()
        {
            if (this.username.Text.Length != 0 && this.password.Password.Length != 0 && this.IsValidUsername == true && this.IsValidPassword == true)
            {
                loginButton.IsEnabled = true;
            }
            else
            {
                loginButton.IsEnabled = false;
            }
        }

        private void username_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.IsValidUsername = true;
            checkUsernameAndPasswordInput();
        }

        private void password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            this.IsValidPassword = true;    
            checkUsernameAndPasswordInput();
        }

        private void loginButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e = null)
        {
            MessageFormat msg = Communicator.Instance.sendLogIn(this.username.Text, this.password.Password);


            if (msg.code == 201)
            {
                App.Current.Properties["username"] = this.username.Text;
                App.Current.Properties["initials"] = this.username.Text[0].ToString().ToUpper();
                App.Current.Properties["profilepicturergb"] = Randomizer.Instance.hexColorForUserBacground();

                DataUser.Instance.Name = App.Current.Properties["username"].ToString();
                DataUser.Instance.Initials = App.Current.Properties["initials"].ToString();
                DataUser.Instance.ProfilePictureRGB = App.Current.Properties["profilepicturergb"].ToString();

                App.Current.Properties["MainWindow"] = new MainWindow();
                this.window.Close();
            }
            else
            {
                MessageBox.Show(msg.content, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void signUpText_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                MessageBox.Show("On working...", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void password_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                loginButton_MouseLeftButtonDown(sender);
            }
        }
    }
}