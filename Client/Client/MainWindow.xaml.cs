﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //start async recive
            Communicator.Instance.StartAsyncRecive();

            //get all data from server
            Communicator.Instance.sendGetAllFriends();
            Communicator.Instance.sendGetAllFriendRequest();

            Voip.Instance.startVOIP();

            this.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Voip.Instance.disposeVOIP();
            Console.WriteLine("disposeVOIP before close");
        }
    }

}