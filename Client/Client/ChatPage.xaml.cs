﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for ChatPage.xaml
    /// </summary>
    public partial class ChatPage : Page
    {
        private Boolean WasShiftLast { get; set; }

        private ChatListItemViewModel ChatPageData { get; set; }
        public ChatPage(ChatListItemViewModel chatData)
        {
            InitializeComponent();

            this.Name.Text = chatData.Name;

            this.sv.DataContext = chatData;

            this.ChatPageData = chatData;
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = ((TextBox)sender);

            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                this.WasShiftLast = true;
            }
            else if (e.Key == Key.Enter)
            {
                if (WasShiftLast)
                {
                    tb.Text += "\n";
                    tb.CaretIndex = tb.Text.Length;
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(tb.Text) && tb.Text.Length > 0)
                    {
                        string guid = Guid.NewGuid().ToString();

                        DataChat.Instance.addMessageFromMe(this.ChatPageData.Name, tb.Text.TrimStart(), guid);

                        this.sv.ScrollToEnd();

                        Communicator.Instance.sendMessageRequest(this.ChatPageData.Name, tb.Text.TrimStart(), guid);

                        tb.Text = "";
                    }
                }

            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                this.WasShiftLast = false;
            }
        }
    }
}
