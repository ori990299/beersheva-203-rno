﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class ChatMessageViewModel : BaseViewModel
    {

        /// <summary>
        /// The display name of this chat
        /// </summary>
        private string _name { get; set; }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// The latest mesage from this chat
        /// </summary>
        private string _message;
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        /// <summary>
        /// The Initials to show for the profile picture background
        /// </summary>
        private string _initials { get; set; }
        public string Initials
        {
            get
            {
                return _initials;
            }
            set
            {
                _initials = value;
                OnPropertyChanged("Initials");
            }
        }

        /// <summary>
        /// The RGB values (in hex) for te background color of the profile picture
        /// Fore exmeple FF00FF Red and Blue mixed
        /// </summary>
        private string _profilepicturergb { get; set; }
        public string ProfilePictureRGB
        {
            get
            {
                return _profilepicturergb;
            }
            set
            {
                _profilepicturergb = value;
                OnPropertyChanged("ProfilePictureRGB");
            }
        }

        private string _messagefontrgb { get; set; }
        public string MessageFontRGB
        {
            get
            {
                return _messagefontrgb;
            }
            set
            {
                _messagefontrgb = value;
                OnPropertyChanged("MessageFontRGB");
            }
        }
    }
}
