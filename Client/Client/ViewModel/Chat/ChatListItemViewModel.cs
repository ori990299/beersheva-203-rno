﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;

namespace Client
{
    /// <summary>
    /// View odel for each chat list item in the overview chat list
    /// </summary>
    public class ChatListItemViewModel : BaseViewModel
    {
        public ChatListItemViewModel()
        {

        }

        public ChatListItemViewModel(string name)
        {
            this.Name = name;
            this.Initials = name[0].ToString().ToUpper();
            this.Message = "...";
            this.ProfilePictureRGB = Randomizer.Instance.hexColorForUserBacground();
            this.NewContentAvailable = false;
            this.ChatMessages = new ObservableCollectionEx<ChatMessageViewModel>(new List<ChatMessageViewModel>());
            try
            {
                this.ShowChatPage = new ChatPage(this);
            }
            catch(Exception e)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    this.ShowChatPage = new ChatPage(this);
                }));
            }
        }

        /// <summary>
        /// The display name of this chat
        /// </summary>
        private string _name { get; set; }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// The latest mesage from this chat
        /// </summary>
        private string _message;
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        /// <summary>
        /// The Initials to show for the profile picture background
        /// </summary>
        private string _initials { get; set; }
        public string Initials
        {
            get
            {
                return _initials;
            }
            set
            {
                _initials = value;
                OnPropertyChanged("Initials");
            }
        }

        /// <summary>
        /// The RGB values (in hex) for te background color of the profile picture
        /// Fore exmeple FF00FF Red and Blue mixed
        /// </summary>
        private string _profilepicturergb { get; set; }
        public string ProfilePictureRGB
        {
            get
            {
                return _profilepicturergb;
            }
            set
            {
                _profilepicturergb = value;
                OnPropertyChanged("ProfilePictureRGB");
            }
        }

        /// <summary>
        /// True if there are unread messages in this chat
        /// </summary>
        private bool _newcontentavailable { get; set; }
        public bool NewContentAvailable
        {
            get
            {
                return _newcontentavailable;
            }
            set
            {
                _newcontentavailable = value;
                OnPropertyChanged("NewContentAvailable");
            }
        }

        /// <summary>
        /// True if this i tem is currently selected
        /// </summary>
        private bool _isiselected { get; set; }
        public bool IsISelected
        {
            get
            {
                return _isiselected;
            }
            set
            {
                _isiselected = value;
                OnPropertyChanged("IsISelected");
            }
        }

        /// <summary>
        /// Chat Messages
        /// </summary>
        private ObservableCollectionEx<ChatMessageViewModel> _chatmessages;
        public ObservableCollectionEx<ChatMessageViewModel> ChatMessages
        {
            get
            {
                return _chatmessages;
            }
            set
            {
                _chatmessages = value;
                _chatmessages.CollectionChanged += ChatMessages_CollectionChanged;
                OnPropertyChanged("ChatMessages");
            }
        }
        private void ChatMessages_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("ChatMessages");
        }

        /// <summary>
        /// the page that displayes the chat
        /// </summary>
        public ChatPage ShowChatPage;
    }
}
