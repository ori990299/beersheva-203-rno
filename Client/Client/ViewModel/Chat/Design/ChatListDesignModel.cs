﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    /// <summary>
    /// The design data for a <see cref="ChatListViewModel"/>
    /// </summary>
    public class ChatListDesignModel : ChatListViewModel
    {
        /// <summary>
        /// instance for the design model
        /// </summary>

        public static ChatListDesignModel Instance => new ChatListDesignModel();

        #region constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ChatListDesignModel()
        {
            Items = new List<ChatListItemViewModel>
            {
                new ChatListItemViewModel
                {
                    Initials = "PO",
                    Name = "Pop",
                    Message = "Forward. Never backwards.",
                    ProfilePictureRGB = "02c1e3",
                    NewContentAvailable = false,
                },
                new ChatListItemViewModel
                {
                    Initials = "LC",
                    Name = "Luke Cage",
                    Message = "coffe?",
                    ProfilePictureRGB = "fce803",
                    NewContentAvailable = true,
                },
                new ChatListItemViewModel
                {
                    Initials = "TS",
                    Name = "Tony Stark",
                    Message = "I am Iron Man.",
                    ProfilePictureRGB = "c91af0",
                },
                new ChatListItemViewModel
                {
                    Initials = "PP",
                    Name = "Peter Parker",
                    Message = "Okay. Cap. Captain. Big fan. I'm Spider-Man",
                    ProfilePictureRGB = "02e3a0",
                    IsISelected = true,
                },
                new ChatListItemViewModel
                {
                    Initials = "NR",
                    Name = "Natasha Romanoff",
                    Message = "Let Me Put You On Hold.",
                    ProfilePictureRGB = "028af2",
                },
                new ChatListItemViewModel
                {
                    Initials = "PO",
                    Name = "Pop",
                    Message = "Forward. Never backwards.",
                    ProfilePictureRGB = "02c1e3",
                },
                new ChatListItemViewModel
                {
                    Initials = "LC",
                    Name = "Luke Cage",
                    Message = "coffe?",
                    ProfilePictureRGB = "fce803",
                },
                new ChatListItemViewModel
                {
                    Initials = "TS",
                    Name = "Tony Stark",
                    Message = "I am Iron Man.",
                    ProfilePictureRGB = "c91af0",
                },
                new ChatListItemViewModel
                {
                    Initials = "PP",
                    Name = "Peter Parker",
                    Message = "Okay. Cap. Captain. Big fan. I'm Spider-Man",
                    ProfilePictureRGB = "02e3a0",
                },
                new ChatListItemViewModel
                {
                    Initials = "NR",
                    Name = "Natasha Romanoff",
                    Message = "Let Me Put You On Hold.",
                    ProfilePictureRGB = "028af2",
                },
            };
        }
        #endregion
    }
}
