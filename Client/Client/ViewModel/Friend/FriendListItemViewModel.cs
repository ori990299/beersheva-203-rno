﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class FriendListItemViewModel : BaseViewModel
    {
        /// <summary>
        /// The display name of this friend
        /// </summary>
        public string _name { get; set; }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        /// <summary>
        /// The Initials to show for the profile picture background
        /// </summary>
        public string _initials { get; set; }
        public string Initials
        {
            get
            {
                return _initials;
            }
            set
            {
                _initials = value;
                OnPropertyChanged("Initials");
            }
        }

        /// <summary>
        /// The RGB values (in hex) for te background color of the profile picture
        /// Fore exmeple FF00FF Red and Blue mixed
        /// </summary>
        public string _profilepicturergb { get; set; }
        public string ProfilePictureRGB
        {
            get
            {
                return _profilepicturergb;
            }
            set
            {
                _profilepicturergb = value;
                OnPropertyChanged("ProfilePictureRGB");
            }
        }
    }
}
