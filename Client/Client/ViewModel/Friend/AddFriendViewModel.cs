﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class AddFriendViewModel : BaseViewModel
    {

        private static AddFriendViewModel instance = null;

        private static readonly object padlock = new object();
        public static AddFriendViewModel Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new AddFriendViewModel();
                    }
                    return instance;
                }
            }
        }

        private string _message;
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        private string _colorrgb;
        public string ColorRGB
        {
            get
            {
                return _colorrgb;
            }
            set
            {
                _colorrgb = value;
                OnPropertyChanged("ColorRGB");
            }
        }

        private AddFriendViewModel()
        {
            Message = "Loading...";
            ColorRGB = "000000";
        }

    }
}
