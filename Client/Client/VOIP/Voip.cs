﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NAudio.Wave;

namespace Client
{
    class Voip
    {
        /// <summary>
        /// Instance
        /// </summary>
        private static Voip instance = null;
        private static readonly object padlock = new object();
        public static Voip Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Voip();
                    }
                    return instance;
                }
            }
            set { instance = value; }

        }

        /// <summary>
        /// server udp data
        /// </summary>
        private UdpClient client { get; set; }
        public string serverIP { get; set; }
        public int port { get; set; }

        /// <summary>
        /// voip data
        /// </summary>
        private WaveFormat format { get; set; }
        private bool IsInCall { get; set; }
        private byte[] data { get; set; }
        private WaveIn sourceStreamInput { get; set; }
        private DirectSoundOut waveOutput { get; set; }
        private BufferedWaveProvider provider { get; set; }
        private Thread reciveAudio { get; set; }

        /// <summary>
        /// public data
        /// </summary>
        public bool callActivitySatus { get { return IsInCall; } }
        private Voip()
        {
            this.serverIP = "176.230.211.127";
            this.port = 9050;

            this.client = new UdpClient();
            this.client.Connect(this.serverIP, this.port);

            this.format = new WaveFormat(44400, 2);
            this.data = new byte[17760];
            this.IsInCall = false;

            //Audio input(the data that needs to be sent to the server)
            this.sourceStreamInput = new WaveIn();
            this.sourceStreamInput.WaveFormat = format;//sets the wave format
            this.sourceStreamInput.DeviceNumber = 0;// sets input device to default

            this.sourceStreamInput.DataAvailable += (object sender, WaveInEventArgs args) =>
            {
                if (IsInCall)
                {
                    this.client.Send(args.Buffer, args.BytesRecorded);
                }
            };

            //Audio output(the data that needs to transfer to wave and play)
            this.provider = new BufferedWaveProvider(this.format);
            this.provider.DiscardOnBufferOverflow = true;

            this.waveOutput = new DirectSoundOut();
            this.waveOutput.Init(provider);

            this.reciveAudio = new Thread(startReciveAudio);
            this.reciveAudio.Start();
        }

        public void startVOIP()
        {
            if (this.IsInCall == false)
            {
                this.sourceStreamInput.StartRecording();
                this.provider.ClearBuffer();
                this.waveOutput.Play();
                this.IsInCall = true;
            }
        }

        public void stopVOIP()
        {
            if (this.IsInCall == true)
            {
                this.IsInCall = false;
                this.sourceStreamInput.StopRecording();
                this.waveOutput.Stop();
                this.provider.ClearBuffer();
            }
        }

        public void changeInputVOIP(int SelectedInput)
        {
            //if (this.IsInCall)
            //{
            //    this.sourceStreamInput.StopRecording();
            //    this.sourceStreamInput.DeviceNumber = SelectedInput;
            //    this.sourceStreamInput.StartRecording();
            //}
            //else
            //{
            //    this.sourceStreamInput.DeviceNumber = SelectedInput;
            //}
        }

        private void startReciveAudio()
        {
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            while(true)
            {
                if (this.IsInCall)
                {
                    try
                    {
                        this.data = this.client.Receive(ref RemoteIpEndPoint);
                        this.provider.AddSamples(data, 0, data.Length);
                    }
                    catch
                    {
                        //pass (try next time)
                    }
                }
            }
        }

        public void disposeVOIP()
        {
            this.IsInCall = false;
            this.sourceStreamInput.StopRecording();
            this.waveOutput.Stop();
            this.provider.ClearBuffer();
            this.client.Close();
            this.reciveAudio.Abort();

            this.sourceStreamInput.Dispose();
            this.waveOutput.Dispose();
            this.client.Dispose();
        }
    }
}
